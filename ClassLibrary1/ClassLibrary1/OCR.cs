﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Tesseract;


namespace ClassLibrary1
{
    public class OCR
    {
        public string TesseractOCR(Image testimage, string tessdatalocation)
        {
            string text = string.Empty;
            float meanConfidence = 0;
            try
            {
                using (var engine = new TesseractEngine(tessdatalocation + "/ClassLibrary1/ClassLibrary1/tessdata", "eng"))
                {
                    var mem = ImageToByte2(testimage);

                    using (var img = Pix.LoadTiffFromMemory(mem))
                    {
                        using (var page = engine.Process(img))
                        {
                            text = page.GetText();
                            meanConfidence = page.GetMeanConfidence();
                            //Console.WriteLine("Mean confidence: {0}", meanConfidence);

                            //Console.WriteLine("Text (GetText): \r\n{0}", text);
                        }
                    }
                }
            }
            

            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
                Console.WriteLine("Unexpected Error: " + e.Message);
                Console.WriteLine("Details: ");
                Console.WriteLine(e.ToString());
            }
            return ("Text is" + text +" Mean confidence is " + meanConfidence.ToString());

        }

        public static byte[] ImageToByte2(Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Tiff);
                return stream.ToArray();
            }
        }
    }
}
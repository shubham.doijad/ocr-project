using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;

namespace OCRConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var directory = Directory.GetCurrentDirectory();
            var directorylist = directory.Split('\\');
            var testdata = string.Empty;
            foreach (var item in directorylist)
            {
                if (item != "OCRConsole")
                {
                    testdata = testdata + item + "/";
                }
                else
                {
                    break;
                }
            }
            var testingfile = testdata + "RegionalExpress";

            File.WriteAllText(testdata + "RegionalExpress/output.txt", String.Empty);

            for (int i = 1; i <= 7; i++)
            {
                var sda = new OCR();

                
                #region Remove black pixels
                // Bitmap myBitmap = new Bitmap("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/Tigerair/captcha1.png");
                // RemoveBlackPixels(myBitmap);
                #endregion

                #region General method to clear image values
                //ClearingImageNoise();
                #endregion

                #region Remove gray pixels
                // Bitmap imgEasy = new Bitmap("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/Tigerair/captcha1.png");
                // RemoveGrayPixels(imgEasy);
                #endregion

                #region Image Sharpening
                //Bitmap image = new Bitmap("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/RegionalExpress/captcha12.png");
                //SharpenImage(image);
                #endregion

                #region Adjust Image Size
                var testImagePath = testingfile + $"/captcha{i}.png";
                int newHeight = 400; int newWidth = 1000;
                var adjustedimageBitmap = AdjustImageSize(testImagePath, testingfile, newHeight, newWidth);
                #endregion

                #region invert Image
                //Bitmap sharpenImage = new Bitmap("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/Tigerair/captcha16.png");

                //Bitmap srcBmp = new Bitmap(sharpenImage);

                //srcBmp.Save("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/Tigerair/captcha17.png", ImageFormat.Png);

                //sharpenImage = new Bitmap("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/Tigerair/captcha17.png");

                //InvertImage(sharpenImage);
                #endregion

                #region Set Pixel Value
                //Bitmap c = new Bitmap(testImagePath);
                //Bitmap d;

                //d = SetPixelofImage(c);
                #endregion

                #region Clear Noise
                //Bitmap img = new Bitmap("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/IberiaExpress/captcha1.png");
                //ClearNoise(img);
                #endregion

                #region SetResolution
                //using (Bitmap bitmap = (Bitmap)Image.FromFile(testImagePath))
                //{
                //    using (Bitmap newbitmap = new Bitmap(bitmap))
                //    {
                //        newbitmap.SetResolution(300, 300);
                //        newbitmap.Save("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/captcha9.png", ImageFormat.Png);
                //    }
                //}
                #endregion

                #region Extract Yellow Pixels
                // Bitmap image = new Bitmap("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/IberiaExpress/captcha1.png");
                //for (int i = 0; i < image.Width; i++)
                //{
                //    for (int j = 0; j < image.Height; j++)
                //    {
                //        Color pixel = image.GetPixel(i, j);
                //        if (pixel == Color.Yellow)
                //        {
                //            image.SetPixel(i, j, Color.Black);
                //        }
                //    }
                //}
                //image.Save("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/IberiaExpress/captcha16.png", ImageFormat.Png);
                #endregion

                #region Extract each pixel color
                //Bitmap imag = new Bitmap("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/IberiaExpress/captcha1.png");
                //int threshold = 2;
                //int distance = 9;
                //int rows, cols, row, col, x, y, count;
                //int dhalf = (distance / 2) + 1;
                //int Sqdistance = SQ(distance);
                //rows = imag.Height;
                //cols = imag.Width;
                //Bitmap bitmap0 = (Bitmap)imag.Clone();
                //Bitmap bitmap = new Bitmap(cols, rows);
                //Bitmap outmap = new Bitmap(cols, rows);

                ////convert to grayscale of a single byte
                //bitmap.Save("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/IberiaExpress/captcha17.png", ImageFormat.Png);
                //for (row = 0; row < rows; row++)
                //{
                //    for (col = 0; col < cols; col++)
                //    {
                //        Color pixel = bitmap0.GetPixel(col, row);
                //        byte grayish = (byte)Math.Floor((decimal)(pixel.R + pixel.G + pixel.B) / 3);
                //        bitmap.SetPixel(col, row, Color.FromArgb(grayish, grayish, grayish));
                //    }
                //}
                //bitmap.Save("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/IberiaExpress/captcha17.png", ImageFormat.Png);
                ////check our threshold to set black or white by checking each pixels in a square defined by distance.
                //for (row = 0; row < rows; row++)
                //{
                //    for (col = 0; col < cols; col++)
                //    {
                //        count = 0;
                //        //could optimize here heavily maybe, by only checking inside a circle rather than square+dist.
                //        for (x = Math.Max(col - dhalf, 0); x < Math.Min(col + dhalf, cols); x++)
                //            for (y = Math.Max(row - dhalf, 0); y < Math.Min(row + dhalf, rows); y++)
                //            {
                //                //if inside the square and pixel color is not black count one more not black pixel
                //                if ((Sqdistance > DISTANCE(col, row, x, y)) && ((bitmap.GetPixel(x, y).R) > 0)) //this second condition is killing a lot the speed to begin.
                //                    count++;
                //            }

                //        //if too much count of not black pixel, set it white.
                //        if (count >= threshold)
                //            outmap.SetPixel(col, row, Color.FromArgb(255, 255, 255));
                //        else
                //            outmap.SetPixel(col, row, Color.FromArgb(0, 0, 0));

                //    }
                //}
                //outmap.Save("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/IberiaExpress/captcha16.png", ImageFormat.Png);
                #endregion
                var tessdatalocation = testdata + "ClassLibrary1/ClassLibrary1/tessdata";

                var value = sda.TesseractOCR(adjustedimageBitmap, testdata);

                File.AppendAllText(testdata + "RegionalExpress/output.txt", value+Environment.NewLine);
            }
        }

        private static void ClearNoise(Bitmap img)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height);

            ColorMatrix grayMatrix = new ColorMatrix(
                    new float[][] {
            new float[] { 0.299f, 0.299f, 0.299f, 0, 0  },
            new float[] { 0.587f, 0.587f, 0.587f, 0, 0  },
            new float[] { 0.114f, 0.114f, 0.114f, 0, 0 },
            new float[] { 0, 0, 0, 1, 0 },
            new float[] { 0, 0, 0, 0, 1}
                    });


            using (Graphics g = Graphics.FromImage(bmp))
            {
                using (ImageAttributes ia = new ImageAttributes())
                {
                    ia.SetColorMatrix(grayMatrix);
                    ia.SetThreshold(0.5f);

                    g.DrawImage(img, new Rectangle(0, 0, img.Width, img.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, ia);
                }

            }
            bmp.Save("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/IberiaExpress/captcha17.png", ImageFormat.Png);
        }

        private static Bitmap SetPixelofImage(Bitmap c)
        {
            Bitmap d;
            for (int x = 0; x < c.Width; x++)
            {
                for (int y = 0; y < c.Height; y++)
                {
                    Color pixelColot = c.GetPixel(x, y);
                    Color newColor = Color.FromArgb(pixelColot.R, 0, 0);
                    c.SetPixel(x, y, newColor);
                }
            }
            d = c;

            d.Save("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/captcha10.png", ImageFormat.Png);
            return d;
        }

        private static void InvertImage(Bitmap sharpenImage)
        {
            for (int y = 0; (y <= (sharpenImage.Height - 1)); y++)
            {
                for (int x = 0; (x <= (sharpenImage.Width - 1)); x++)
                {
                    Color inv = sharpenImage.GetPixel(x, y);
                    inv = Color.FromArgb(255, (255 - inv.R), (255 - inv.G), (255 - inv.B));
                    sharpenImage.SetPixel(x, y, inv);
                }
            }
            sharpenImage.Save("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/Tigerair/captcha15.png", ImageFormat.Png);
        }

        private static void SharpenImage(Bitmap image)
        {
            Bitmap sharpenImage = new Bitmap(image.Width, image.Height);

            int filterWidth = 3;
            int filterHeight = 3;
            int w = image.Width;
            int h = image.Height;
            double[,] filter = new double[filterWidth, filterHeight];

            filter[0, 0] = filter[0, 1] = filter[0, 2] = filter[1, 0] = filter[1, 2] = filter[2, 0] = filter[2, 1] = filter[2, 2] = -1;
            filter[1, 1] = 9;

            double factor = 1.0;
            double bias = 0.0;

            Color[,] result = new Color[image.Width, image.Height];
            for (int x = 0; x < w; ++x)
            {
                for (int y = 0; y < h; ++y)
                {
                    double red = 0.0, green = 0.0, blue = 0.0;

                    for (int filterX = 0; filterX < filterWidth; filterX++)
                    {
                        for (int filterY = 0; filterY < filterHeight; filterY++)
                        {
                            int imageX = (x - filterWidth / 2 + filterX + w) % w;
                            int imageY = (y - filterHeight / 2 + filterY + h) % h;

                            Color imageColor = image.GetPixel(imageX, imageY);

                            red += imageColor.R * filter[filterX, filterY];
                            green += imageColor.G * filter[filterX, filterY];
                            blue += imageColor.B * filter[filterX, filterY];
                        }
                        int r = Math.Min(Math.Max((int)(factor * red + bias), 0), 255);
                        int g = Math.Min(Math.Max((int)(factor * green + bias), 0), 255);
                        int b = Math.Min(Math.Max((int)(factor * blue + bias), 0), 255);

                        result[x, y] = Color.FromArgb(r, g, b);

                    }
                }
            }
            for (int i = 0; i < w; ++i)
            {
                for (int j = 0; j < h; ++j)
                {
                    sharpenImage.SetPixel(i, j, result[i, j]);
                }
            }

            sharpenImage.Save("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/RegionalExpress/captcha13.png", ImageFormat.Png);
        }

        private static void RemoveGrayPixels(Bitmap imgEasy)
        {
            var img2dEasy = new Color[imgEasy.Width, imgEasy.Height];
            for (int i = 0; i < imgEasy.Width; i++)
            {
                for (int j = 0; j < imgEasy.Height; j++)
                {
                    Color pixel = imgEasy.GetPixel(i, j);

                    // Preprocessing
                    if (pixel.R == pixel.G && pixel.B == pixel.G)
                    {
                        // Convert all greys to white.
                        pixel = Color.White;
                    }

                    img2dEasy[i, j] = pixel;
                }
            }

            var imgOut = new Bitmap(imgEasy.Width, imgEasy.Height);
            for (int i = 0; i < imgEasy.Width; i++)
            {
                for (int j = 0; j < imgEasy.Height; j++)
                {
                    imgOut.SetPixel(i, j, img2dEasy[i, j]);
                }
            }
            var stream = new MemoryStream();
            imgOut.Save("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/Tigerair/captcha16.png", ImageFormat.Png);
        }

        private static Image AdjustImageSize(string testImagePath,string testingImagefolder, int newHeight, int newWidth)
        {
            System.Net.WebRequest request =System.Net.WebRequest.Create("https://secure.rex.com.au/RexOBE/Captcha.ashx");
            System.Net.WebResponse response = request.GetResponse();
            Stream responseStream = response.GetResponseStream();

            Bitmap srcBmp = new Bitmap(testImagePath);

            float ratio = 1;
            float minSize = Math.Min(newHeight, newWidth);

            if (srcBmp.Width > srcBmp.Height)
            {
                ratio = minSize / srcBmp.Width;
            }
            else
            {
                ratio = minSize / srcBmp.Height;
            }

            SizeF newSize = new SizeF(srcBmp.Width * ratio, srcBmp.Height * ratio);
            Bitmap target = new Bitmap((int)newSize.Width, (int)newSize.Height);

            using (Graphics graphics = Graphics.FromImage(target))
            {
                graphics.CompositingQuality = CompositingQuality.HighSpeed;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.DrawImage(srcBmp, 10, 10, newSize.Width, newSize.Height);

            }
            return target;
        }

        private static void ClearingImageNoise()
        {
            var imgBmp = new Bitmap(@"C:\Users\Shubham.doijad\Pictures\Saved Pictures\Captchas\RegionalExpress\captcha3.png");
            //var scaleVal = 50;
            imgBmp.Save(@"C:\Users\Shubham.doijad\Pictures\Saved Pictures\Captchas\RegionalExpress\captcha20.png", ImageFormat.Png);
            imgBmp = new Bitmap(@"C:\Users\Shubham.doijad\Pictures\Saved Pictures\Captchas\RegionalExpress\captcha20.png");
            SetPixelColor(imgBmp, false);
            imgBmp.Save(@"C:\Users\Shubham.doijad\Pictures\Saved Pictures\Captchas\RegionalExpress\captcha12.png", ImageFormat.Png);
            //using (var imgBmpScaled = new Bitmap(imgBmp, new Size((int)(imgBmp.Width * scaleVal), (int)(imgBmp.Height * scaleVal))))
            //{
            //    imgBmp.Save(@"C:\Users\Shubham.doijad\Pictures\Saved Pictures\Captchas\RegionalExpress\captcha11.png", ImageFormat.Png);//after scaled
            //    SetPixelColor(imgBmpScaled);
            //    imgBmp.Save(@"C:\Users\Shubham.doijad\Pictures\Saved Pictures\Captchas\RegionalExpress\captcha15.png", ImageFormat.Png);//after the second cleaning
            //}
        }

        private static void RemoveBlackPixels(Bitmap myBitmap, bool value = true)
        {
            if (value == true)
            {
                myBitmap.MakeTransparent(myBitmap.GetPixel(0, 0));

                float limit = 0.3f;
                for (int i = 0; i < myBitmap.Width; i++)
                {
                    for (int j = 0; j < myBitmap.Height; j++)
                    {
                        Color c = myBitmap.GetPixel(i, j);
                        if (c.GetBrightness() < limit)
                        {
                            myBitmap.SetPixel(i, j, Color.White);
                        }
                    }
                }
                myBitmap.Save("C:/Users/Shubham.doijad/Pictures/Saved Pictures/Captchas/Tigerair/captcha16.png", ImageFormat.Png);
            }
            
        }

        private static void SetPixelColor(Bitmap imgBmp, bool hasBeenCleared = true) //type 0 dafault, image has has been cleared.
        {
            var bgColor = Color.White;
            var textColor = Color.Black;
            for (var x = 0; x < imgBmp.Width; x++)
            {
                for (var y = 0; y < imgBmp.Height; y++)
                {
                    var pixel = imgBmp.GetPixel(x, y);
                    var isCloserToWhite = hasBeenCleared ? ((pixel.R + pixel.G + pixel.B) / 3) > 180 : ((pixel.R + pixel.G + pixel.B) / 3) > 120;
                    imgBmp.SetPixel(x, y, isCloserToWhite ? bgColor : textColor);   
                }
            }
        }

        private static int DISTANCE(int a, int b, int c, int d)
        {
            return (SQ(a - c) + SQ(b - d));
        }

        private static int SQ(int distance)
        {
            return (distance * distance);
        }


    }
}
